const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

// chức năng thêm sinh viên
var dssv = [];
//Lấy thông tin từ localstorage

var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
if (dssvJson != null) {
  dssv = JSON.parse(dssvJson);
  //  array khi convert thành json sẽ mất function, ta sẽ map lại
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];
    dssv[index] = new SinhVien(
      sv.ma,
      sv.ten,
      sv.email,
      sv.matKhau,
      sv.toan,
      sv.ly,
      sv.hoa
    );
  }
  renderDSSV(dssv);
}
function themSV() {
  var newSv = LayThongTinTuForm();
  // console.log('newSv: ', newSv);

  //check masv
  var isValid =
    validator.kiemTraRong(
      newSv.ma,
      "spanMaSV",
      "Mã sinh viên không được để trống"
    ) &&
    validator.kiemTraDoDai(
      newSv.ma,
      "spanMaSV",
      "Mã sinh viên phải chứa 4 ký tự",
      4,
      6
    ) &&
    validator.kiemTraSo(newSv.ma, "spanMaSV", "Mã sinh viên không hợp lệ");
  // check tensv
  isValid &=
    validator.kiemTraRong(
      newSv.ten,
      "spanTenSV",
      "Tên sinh viên không được để trống"
    ) &&
    validator.kiemTraChu(newSv.ten, "spanTenSV", "Tên sinh viên không hợp lệ");
  // check email
  isValid &=
    validator.kiemTraRong(
      newSv.email,
      "spanEmailSV",
      "Email không được để trống"
    ) &&
    validator.kiemTraEmail(newSv.email, "spanEmailSV", "Email không hợp lệ");
  // check matkhau
  isValid &=
    validator.kiemTraRong(
      newSv.matKhau,
      "spanMatKhau",
      "Mật khẩu không được để trống"
    ) &&
    validator.kiemTraDoDai(
      newSv.matKhau,
      "spanMatKhau",
      "Mật khẩu không hợp lệ",
      6,
      10
    ) &&
    validator.kiemTraMatKhau(
      newSv.matKhau,
      "spanMatKhau",
      "Mật khẩu không hợp lệ"
    );
  //check diemToan
  isValid &=
    validator.kiemTraRong(
      newSv.toan,
      "spanToan",
      "Điểm toán không được để trống"
    ) &&
    validator.kiemTraDoDai(
      newSv.toan,
      "spanToan",
      "Điểm toán không hợp lệ",
      1,
      2
    ) &&
    validator.kiemTraDiem(newSv.toan, "spanToan", "Điểm toán không hợp lệ") &&
    validator.kiemTraSo(newSv.toan, "spanToan", "Điểm toán không hợp lệ");
  //check diemLy
  isValid &=
    validator.kiemTraRong(newSv.ly, "spanLy", "Điểm lý không được để trống") &&
    validator.kiemTraDoDai(newSv.ly, "spanLy", "Điểm lý không hợp lệ", 1, 2) &&
    validator.kiemTraDiem(newSv.ly, "spanLy", "Điểm lý không hợp lệ") &&
    validator.kiemTraSo(newSv.ly, "spanLy", "Điểm lý không hợp lệ");
  //check diemHoa
  isValid &=
    validator.kiemTraRong(
      newSv.hoa,
      "spanHoa",
      "Điểm hoá không được để trống"
    ) &&
    validator.kiemTraDoDai(
      newSv.hoa,
      "spanHoa",
      "Điểm hoá không hợp lệ",
      1,
      2
    ) &&
    validator.kiemTraDiem(newSv.hoa, "spanHoa", "Điểm hoá không hợp lệ") &&
    validator.kiemTraSo(newSv.hoa, "spanHoa", "Điểm hoá không hợp lệ");

  if (isValid) {
    dssv.push(newSv);
    //Tạo json
    var dssvJson = JSON.stringify(dssv);
    //Lưu json
    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
    renderDSSV(dssv);
  }
}

function xoaSinhVien(id) {
  // var index = dssv.findIndex(function (sv) {
  //   return (sv.ma = id);
  // });
  var index = timKiemViTri(id, dssv);
  // tìm thấy vị trí
  if (index != -1) {
    dssv.splice(index, 1);
    var dssvJson = JSON.stringify(dssv);
    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
    renderDSSV(dssv);
  }
}

function suaSinhVien(id) {
  var index = timKiemViTri(id, dssv);
  if (index != -1) {
    var sv = dssv[index];
    showThongTinTuForm(sv);
    document.getElementById("txtMaSV").readOnly = true;
    document.querySelector("#formQLSV button:first-child").disabled = true;
  }
}

function updateSV() {
  themSV();
  document.getElementById("txtMaSV").readOnly = false;
  var id = document.querySelector("#txtMaSV").value;
  xoaSinhVien(id);
}

function resetSV() {
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
  document.getElementById("txtMaSV").readOnly = false;
}

function searchSV() {
  var id = document.querySelector("#txtSearch").value;
  var index = timKiemViTriTheoTen(id, dssv);
  console.log("id: ", id);
  console.log("index: ", index);
  if (index != -1) {
    var sv = dssv[index];
    var trContent = `<tr>
      <td>${sv.ma}</td>
      <td>${sv.ten}</td>
      <td>${sv.email}</td>
      <td>${sv.tinhDTB()}</td>
      <td>
      <button onclick="xoaSinhVien(${
        sv.ma
      })" class="btn btn-danger">Xoá</button>
      <button onclick="suaSinhVien(${
        sv.ma
      })" class="btn btn-warning">Sửa</button>
      </td>
      </tr>`;
  } else {
    trContent = `<tr>
      <td>Không tìm thấy</td>
    </tr>`;
  }
  document.querySelector("#tbodySinhVien").innerHTML = trContent;
}
