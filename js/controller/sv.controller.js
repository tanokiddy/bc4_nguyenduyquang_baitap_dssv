function LayThongTinTuForm() {
  const maSV = document.querySelector("#txtMaSV").value;
  const tenSV = document.querySelector("#txtTenSV").value;
  const email = document.querySelector("#txtEmail").value;
  const matKhau = document.querySelector("#txtPass").value;
  const diemToan = document.querySelector("#txtDiemToan").value;
  const diemLy = document.querySelector("#txtDiemLy").value;
  const diemHoa = document.querySelector("#txtDiemHoa").value;

  return new SinhVien(maSV, tenSV, email, matKhau, diemToan, diemLy, diemHoa);
}
//render array sv ra giao dien
function renderDSSV(svArr) {
  // contentHTML: chuỗi chứa các thẻ <tr></tr>
  var contentHTML = "";
  for (var i = 0; i < svArr.length; i++) {
    var sv = svArr[i];
    //trContent: thẻ tr trong mỗi lần lặp
    var trContent = `<tr>
    <td>${sv.ma}</td>
    <td>${sv.ten}</td>
    <td>${sv.email}</td>
    <td>${sv.tinhDTB()}</td>
    <td>
    <button onclick="xoaSinhVien(${sv.ma})" class="btn btn-danger">Xoá</button>
    <button onclick="suaSinhVien(${sv.ma})" class="btn btn-warning">Sửa</button>
    </td>
    </tr>`;
    contentHTML += trContent;
  }
  document.querySelector("#tbodySinhVien").innerHTML = contentHTML;
  //tbodySinhVien
}

function timKiemViTri(id, dssv) {
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];
    if (sv.ma == id) {
      return index;
    }
  }
  // ko tìm thấy
  return -1;
}

function showThongTinTuForm(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
}

function timKiemViTriTheoTen(id, dssv) {
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];
    if (sv.ten == id) {
      return index;
    }
  }
  // ko tìm thấy
  return -1;
}
